FROM ${ARCH}/debian:stretch as builder

# INSTALL BUILD DEPENDENCIES
RUN apt-get update && apt-get install -y \
	wget \
	xz-utils \
	build-essential \
	cpio \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists

WORKDIR /work

# COPY SOURCE CODE
COPY gdb gdb

# BUILD
WORKDIR out

RUN cd ../gdb/gdb/gdbserver \
	&& sed -i -e 's/srv_linux_thread_db=yes//' ./configure.srv \
	&& ./configure --enable-static --disable-inprocess-agent CXXFLAGS='-fPIC -static' \
	&& make GDBSERVER_LIBS="/usr/lib/gcc/x86_64-linux-gnu/6/libgcc_eh.a /usr/lib/gcc/x86_64-linux-gnu/6/libstdc++.a" \
	&& mkdir -p /work/out/usr/bin \
	&& cp gdbserver /work/out/usr/bin/gdbserver

# 4-BYTE ALIGN CPIO.GZ
RUN cd /work/out \
	&& find -type f | cpio -H newc -ov | xz --keep --check=crc32 --lzma2=dict=512KiB  > /work/addon-gdbserver.cpio.xz \
	&& file_size=`stat -c %s /work/addon-gdbserver.cpio.xz` \
	&& remaining=$(($file_size%4)) \
	&& if [ $remaining = 0 ]; then new_size=$file_size; else new_size=$(($file_size+4-$remaining)); fi \
	&& dd if=/dev/zero of=/work/addon-gdbserver.cpio.xz4 bs=1 count=$new_size \
	&& dd if=/work/addon-gdbserver.cpio.xz of=/work/addon-gdbserver.cpio.xz4 conv=notrunc

FROM ${ARCH}/debian:stretch

# COPY BUILD ARTIFACTS
WORKDIR /out
COPY --from=builder /work/addon-gdbserver.cpio.xz4 /out/addon-gdbserver.cpio.xz4

ENTRYPOINT [ "/bin/tar", "-C", "/out", "-c", "." ]
