FROM alpine as qemu

RUN wget -O /qemu-${QEMU_ARCH}-static https://github.com/multiarch/qemu-user-static/releases/download/v4.1.0-1/qemu-${QEMU_ARCH}-static; \
	chmod a+x /qemu-${QEMU_ARCH}-static

FROM ${ARCH}/alpine:3.10 as sysroot

COPY --from=qemu /qemu-${QEMU_ARCH}-static /usr/bin/

# INSTALL BUILD DEPENDENCIES
RUN apk update && apk --force add \
    && rm -rf /var/cache/apk/*

WORKDIR /work

FROM registry.gitlab.com/pantacor/platform-tools/docker-${LIBC}-cross-${QEMU_ARCH}:master as crossbuilder
COPY --from=sysroot / /sysroot-${QEMU_ARCH}

WORKDIR /work
# COPY SOURCE CODE
COPY parted-3.3 parted-3.3
COPY e2fsprogs-1.45.6 e2fsprogs-1.45.6

# INSTALL MAKE DEPENDENCIES
RUN apt-get update && apt-get install -y \
	cpio \
	xz-utils \
	readline \
	gettext \
	git \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists

RUN cd /work/e2fsprogs-1.45.6/ \
	&& mkdir build \
	&& cd build \
	&& ../e2fsprogs-1.45.6/configure \
	&& make && make install \
	&& make install-libs \
	&& mkdir -p /work/out/usr/bin \
	&& make DESTDIR=/work/out/usr/bin install

RUN cd /work/parted-3.3/parted-3.3/ \
	&& ./configure --host=arm-linux-musleabihf --enable-static --disable-inprocess-agent CXXFLAGS='-fPIC -static' --disable-debug CC=/opt/cross/arm-linux-musleabihf/bin/arm-linux-musleabihf-gcc CXX=/opt/cross/arm-linux-musleabihf/bin/arm-linux-musleabihf-g++\
	&& make \
	&& mkdir -p /work/out/usr/bin \
	&& cp parted /work/out/usr/bin/parted

# 4-BYTE ALIGN CPIO.GZ
RUN cd /work/out \
	&& find -type f | cpio -H newc -ov | xz --keep --check=crc32 --lzma2=dict=512KiB  > /work/addon-parted.cpio.xz \
	&& file_size=`stat -c %s /work/addon-parted.cpio.xz` \
	&& remaining=$(($file_size%4)) \
	&& if [ $remaining = 0 ]; then new_size=$file_size; else new_size=$(($file_size+4-$remaining)); fi \
	&& dd if=/dev/zero of=/work/addon-parted.cpio.xz4 bs=1 count=$new_size \
	&& dd if=/work/addon-parted.cpio.xz of=/work/addon-parted.cpio.xz4 conv=notrunc

FROM alpine:3.10

# COPY BUILD ARTIFACTS
WORKDIR /out
COPY --from=crossbuilder /work/addon-parted.cpio.xz4 addon-parted-${ARCH}.cpio.xz4

ENTRYPOINT [ "/bin/tar", "-C", "/out", "-c", "." ]
